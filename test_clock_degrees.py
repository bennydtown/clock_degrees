import clock_degrees
import unittest


class TestParseTime(unittest.TestCase):

    def test_parse_time_success(self):
        minutes = clock_degrees.parse_time_string('12:20 AM')
        self.assertEqual(minutes, 20)
        minutes = clock_degrees.parse_time_string('12:00 PM')
        self.assertEqual(
            minutes,
            12*clock_degrees.MINUTES_PER_HOUR
        )
        minutes = clock_degrees.parse_time_string('3:05 PM')
        self.assertEqual(
            minutes,
            15*clock_degrees.MINUTES_PER_HOUR + 5
        )

    def test_parse_time_errors(self):
        for bad_string in [
            'INVALID TIME', '3: PM', ':5', 'monkey pants'
        ]:
            try:
                clock_degrees.parse_time_string(bad_string)
            except clock_degrees.ParseTimeException:
                pass
            else:
                self.fail()


class TestMinuteHandDifference(unittest.TestCase):

    def test_minute_hand_difference_null_case(self):
        ## 1:00AM to 1:00AM should be 0 degrees
        degrees = clock_degrees.minute_hand_difference(60, 60)
        self.assertEqual(degrees, 0)

    def test_minute_hand_difference_golden_path(self):
        ## 1:00AM to 2:00AM should be 360 degrees
        degrees = clock_degrees.minute_hand_difference(60, 120)
        self.assertEqual(degrees, 360)
        ## 12:00AM to 2:00AM should be 720 degrees
        degrees = clock_degrees.minute_hand_difference(0, 120)
        self.assertEqual(degrees, 720)
        ## 12:45AM to 1:15AM should be 180 degrees
        degrees = clock_degrees.minute_hand_difference(45, 75)
        self.assertEqual(degrees, 180)

    def test_minute_hand_difference_rollovers(self):
        ## 2:00PM to 10:00AM should be 6000 degrees
        degrees = clock_degrees.minute_hand_difference(
            14*clock_degrees.MINUTES_PER_HOUR,
            10*clock_degrees.MINUTES_PER_HOUR
        )
        self.assertEqual(
            degrees,
            (24-4)*60*clock_degrees.DEGREES_PER_MINUTE
        )


class TestMinuteHandDifferenceFromStrings(unittest.TestCase):

    def test_minute_hand_difference_from_strings(self):
        degrees = clock_degrees.minute_hand_difference_from_strings(
            '12:01 AM', '12:01 AM'
        )
        self.assertEqual(degrees, 0)
        degrees = clock_degrees.minute_hand_difference_from_strings(
            '12:01 AM', '1:02 AM'
        )
        self.assertEqual(degrees, 61*clock_degrees.DEGREES_PER_MINUTE)
        degrees = clock_degrees.minute_hand_difference_from_strings(
            '12:30 PM', '11:00 AM'
        )
        self.assertEqual(
            degrees,
            (22*clock_degrees.MINUTES_PER_HOUR + 30)
            * clock_degrees.DEGREES_PER_MINUTE
        )


if __name__ == '__main__':
    unittest.main()
