# Clock Minute Hand Exercise


## Overview

Allow the user to enter two times in the format “[H]H:MM AM”. Without using 
any built-in date or time routines, calculate the number of degrees the minute hand on 
a clock must travel between the first time and the second time. 

We're assuming that the minute  hand may only move in the clockwise direction. And that
the minute hand always ends up on a perfect minute boundary.

As example solutions, if the two times entered are “10:15 AM” and “12:45 PM” the minute 
hand must travel 900 degrees (2.5 rotations). If the two times entered are “10:00 PM 
and 9:00 PM” the minute hand must travel 8,280 degrees (23 rotations).


## Usage

```
$ python clock_degrees.py
Enter Start Time ([H]H:MM AM format): 10:23 AM
Enter End Time ([H]H:MM AM format): 4:10 PM
The minute hand will travel 2082 degrees.
```


### Running the tests

```
$ python test_clock_degrees.py
```
