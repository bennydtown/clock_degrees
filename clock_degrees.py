import re

HOURS_IN_DAY = 24
HOURS_ON_CLOCK = 12
MINUTES_PER_HOUR = 60
DEGREES_PER_ROTATION = 360
DEGREES_PER_MINUTE = DEGREES_PER_ROTATION/MINUTES_PER_HOUR


class ParseTimeException(Exception):
    pass


def parse_time_string(time_string):

    """ inputs: '[H]H:MM AM' format string
        returns: minutes since midnight """

    try:
        hour, minute, period = re.findall(r"[\w']+", time_string)
        hour = int(hour)
        if hour == 12 and period == 'AM':
            hour = 0
        elif hour != 12 and period == 'PM':
            hour += HOURS_ON_CLOCK
        minute = int(minute)
    except ValueError:
        raise ParseTimeException(
            "Input Error:  Dates must be given in '[H]H:MM AM' format"
        )

    return hour*MINUTES_PER_HOUR + minute


def minute_hand_difference(start_time, end_time):

    """ inputs: start_time (minutes since midnight),
                end_time (minutes since midnight)
        returns: Degrees minute hand must travel from start to end """

    minute_difference = end_time - start_time
    if minute_difference < 0:
        minute_difference += HOURS_IN_DAY * MINUTES_PER_HOUR

    return minute_difference * DEGREES_PER_MINUTE


def minute_hand_difference_from_strings(start_time_string, end_time_string):

    """ inputs: start_time_string   '[H]H:MM AM' format
                end_time_string   '[H]H:MM AM' format
        returns: Degrees minute hand must travel from start to end """

    return minute_hand_difference(
        parse_time_string(start_time_string),
        parse_time_string(end_time_string)
    )


def prompt_user():

    while True:
        try:
            degrees = minute_hand_difference_from_strings(
                raw_input("Enter Start Time ([H]H:MM AM format): "),
                raw_input("Enter End Time ([H]H:MM AM format): "),
            )
        except ParseTimeException, err:
            print err
        else:
            break
    print("The minute hand will travel %d degrees." % degrees)


if __name__ == '__main__':
    prompt_user()
